﻿using System;

namespace dato_basico7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al programa \n");

            /*Crear un programa que pida al usuario a una distancia (en metros)
              y el tiempo necesario para recorrerla (como tres números: horas, minutos, segundos),
              y muestre la velocidad, en metros por segundo, en kilómetros por hora y en millas por hora (pista: 1 milla = 1.609 metros).
             */

            double metros;//metros

            Console.WriteLine("ingrese la cantidad en metros");

            metros = double.Parse(Console.ReadLine());

            double milla = metros * 1.609;//milla

            double segundos = 0, minutos = 0, horas = 0;//tiempo

            Console.WriteLine("ingrese la cantidad de segundos");
            segundos = double.Parse(Console.ReadLine());
            Console.WriteLine("ingrese la cantidad de minutos");
            minutos = double.Parse(Console.ReadLine());
            Console.WriteLine("ingrese la cantidad de horas");
            horas = double.Parse(Console.ReadLine());

            Console.WriteLine("Este es el tiempo que se toma en recorrerla en base a segundos {0} km/h", (metros * 3.6) / segundos);
            Console.WriteLine("Este es el tiempo que se toma en recorrerla en base a minutos es  {0} km/h", (metros * 3.6) / (minutos * 60));
            Console.WriteLine("Este es el tiempo que se toma en recorrerla en base a horas es  {0} km/h", (metros * 3.6) / (horas * 60));
            Console.WriteLine("Esta es la catidad de metros que se toma en recorrerla en base a km {0}", metros * 3.6);
            Console.WriteLine("Esta es la cantidad de metros que se toma en recorrerla en base a millas {0}", metros * 1.60934);

            while (Console.ReadKey().Key != ConsoleKey.Enter) { }
        }
    }
}
