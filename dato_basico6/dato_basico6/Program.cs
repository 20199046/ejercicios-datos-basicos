﻿using System;

namespace dato_basico6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvendos al programa \n");

            //Calcular el área de un círculo, dado su radio (pi * radio al cuadrado).

            double radio, resulta = 0;

            Console.WriteLine("Introduzca el radio");

            radio = float.Parse(Console.ReadLine());
            resulta = Math.PI * (radio * radio);
            Console.WriteLine("{0}", resulta);
            Console.ReadKey();
        }
    }
}
