﻿using System;

namespace datos_basico2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenidos al programa \n");

            //Pedir al usuario dos números enteros largos("long") y mostrar su suma, su resta y su producto.

            long numero1, numero2, resultado1 = 0, resultado2 = 0, resultado3 = 0;

            Console.WriteLine("Ingrese el primer numero");
            numero1 = long.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el segundo numero");
            numero2 = long.Parse(Console.ReadLine());

            Console.WriteLine();

            resultado1 = numero1 + numero2;
            resultado2 = numero1 - numero2;
            resultado3 = numero1 * numero2;

            Console.WriteLine("El resultado de la suma es {0}, la resta es {1} y su producto es {2}", resultado1, resultado2, resultado3);

            while (Console.ReadKey().Key != ConsoleKey.Enter) { }
        }
    }
}
