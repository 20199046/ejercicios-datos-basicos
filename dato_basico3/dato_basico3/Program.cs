﻿using System;

namespace dato_basico3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenidos al programa");

            /*Crear un programa que use tres variables x,y,z. Sus valores iniciales serán 15, -10, 2.147.483.647.
              Se deberá incrementar tres vece el valor de estas variables. 
              Mostrar en pantalla el valor incial de las variables y el valor final obtenido por el programa.
             */ 

            double x = 15, y = -10, z = 2.143483647;

            for (double i = 1; i <= 3; i++)
            {
                Console.WriteLine("{0},{1},{2}",x * i, y * i, z * i);
            }
            
            Console.ReadKey();
        }
    }
}
