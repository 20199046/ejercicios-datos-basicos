﻿using System;

namespace datos_bassico1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al programa \n");

            /*Preguntar al usuario su edad, que se guardará en un "byte". 
            A continuación, se deberá le deberá decir que no aparenta tantos años (por ejemplo, "No aparentas 20 años").
            */

            byte edad;

            Console.WriteLine("Intruduzca su edad");
            edad = byte.Parse(Console.ReadLine());
            Console.WriteLine("No aparentas de {0} años ", edad);

            Console.ReadKey();
        }
    }
}
